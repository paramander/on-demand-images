{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Main where

import qualified Codec.Picture                   as JP
import qualified Codec.Picture.Types             as JP
import qualified Control.Monad.IO.Class          as IO
import qualified Data.Text.Lazy                  as Text
import           Flow
import qualified System.Directory                as Dir
import qualified Vision.Image                    as Friday
import qualified Vision.Image.JuicyPixels        as Friday
import qualified Vision.Primitive                as Friday
import qualified Web.Scotty                      as Scotty

data Size = Thumbnail | Header
    deriving (Enum, Bounded)

-- dimensions given as (width, height)
dimensions :: Size -> (Int, Int)
dimensions Thumbnail = (200, 200)
dimensions Header    = (600, 300)

height :: Size -> Int
height = snd . dimensions

width :: Size -> Int
width  = fst . dimensions

instance Show Size where
    show Thumbnail = "thumbnail"
    show Header    = "header"

instance Scotty.Parsable Size where
    parseParam size = maybe (Left "Invalid size") Right $ lookup size sizes
        where
            sizes :: [(Text.Text, Size)]
            sizes = map (\s -> (Text.pack $ show s, s)) [minBound..maxBound]

main :: IO ()
main = Scotty.scotty 3000 $ do
    Scotty.get "/images/:size/:file" $ do
        size <- Scotty.param "size"
        file <- Scotty.param "file"
        requireOriginal file
        p <- IO.liftIO $ readOrCreateCachedImage size file
        case p of
            Just path -> Scotty.file path
            Nothing -> Scotty.raise "Could not retrieve or create the requested image"

imgDir :: FilePath
imgDir = "./images/"

sizeDir :: Size -> FilePath
sizeDir s = imgDir ++ show s ++ "/"

requireOriginal :: FilePath -> Scotty.ActionM ()
requireOriginal file = do
    doesExist <- IO.liftIO . Dir.doesFileExist $ imgDir ++ file
    if doesExist
        then return ()
        else Scotty.next

readOrCreateCachedImage :: Size -> FilePath -> IO (Maybe FilePath)
readOrCreateCachedImage size file = do
    doesExist <- Dir.doesFileExist path
    if doesExist
        then return $ Just path
        else do
            generated <- generateSize size file
            return $ if generated
                then Just path
                else Nothing
    where
        path = sizeDir size ++ file

generateSize :: Size -> FilePath -> IO Bool
generateSize size file = do
    o <- JP.readJpeg $ imgDir ++ file
    case o of
        Right (JP.ImageYCbCr8 image) -> do
            Dir.createDirectoryIfMissing False $ sizeDir size
            image
                |> JP.convertImage
                |> Friday.toFridayRGB
                |> Friday.crop (rectFor size image)
                |> Friday.delayed
                |> Friday.resize Friday.Bilinear (Friday.ix2 (height size) (width size))
                |> Friday.toJuicyRGB
                |> JP.ImageRGB8
                |> JP.saveJpgImage 100 (sizeDir size ++ file)
            return True
        _ -> return False

rectFor :: Size -> JP.Image a -> Friday.Rect
rectFor size JP.Image{..} = if targetRatio > srcRatio
    then Friday.Rect { rX      = (imageWidth - newWidth) `div` 2
                     , rY      = 0
                     , rWidth  = newWidth
                     , rHeight = imageHeight
                     }
    else Friday.Rect { rX      = 0
                     , rY      = (imageHeight - newHeight) `div` 2
                     , rWidth  = imageWidth
                     , rHeight = newHeight
                     }
    where
        f :: Int -> Double
        f = fromIntegral
        targetRatio = f (height size) / f (width size)
        srcRatio    = f imageHeight / f imageWidth
        newWidth    = floor $ (f imageHeight / f (height size)) * f (width size)
        newHeight   = floor $ (f imageWidth / f (width size)) * f (height size)
